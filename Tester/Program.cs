﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CodeParser;
using System.IO;

namespace Tester {
	class Program {
		static void Main(string[] args) {

			


			//var source = File.ReadAllText("input.pas");

			var source = File.ReadAllText("in_sp.pas");

			var parser = new Parser();
			var @params = new Dictionary<string, object>();
			@params.Add("source", source);
			@params.Add("lang", "pas");
			var resp = parser.Parse(@params);
			resp.ContinueWith(r => {
				if (r.Exception != null)
					foreach (var ex in r.Exception.InnerExceptions) {
						Console.Write(ex.Message);
					}
				else
					Console.WriteLine(r.Result);
				Console.Read();
			}).Wait();
		}

		public async Task<object> foo(string str) {
			var source = File.ReadAllText("in_sp.pas");
			StringBuilder result = new StringBuilder();
			var parser = new Parser();
			var @params = new Dictionary<string, object>();
			@params.Add("source", source);
			@params.Add("lang", "pas");
			return await parser.Parse(@params);
		}
	}
}
