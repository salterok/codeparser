﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeParser {
	class Function : Block {
		public string Name { get; set; }

		public Function()
			: base("function") {

		}
	}
}
