﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeParser {
	interface IParser {
		IEnumerable<Statement> Parse(string source);
	}
}
