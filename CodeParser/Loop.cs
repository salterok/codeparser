﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeParser {
	class Loop : Block {
		public bool IsPostClause { get; set; }

		public Loop(bool isPost = false)
			: base("loop") {
			IsPostClause = isPost;
		}
	}
}
