﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace CodeParser {
	class PascalParser : IParser {
		private const string TOKENS = @"(?<token>(?:(?<=^|\s+|}|;)(?:function|procedure|begin|end|if|else|for|do|while|repeat|until|then|with)(?=\s+|$|\.|;|//|{))|;|//.*$|(?s:{.*?})|'(?:\\'|''|[^']*?)*')";
		private const string DECLARATION = @"\s*(?<type>function|procedure)\s+(?<name>\w+)\s*(?:\(\s*(?:(?<params>(?:var\s+)?\w+(?:\s*,\s*\w+)?\s*:\s*\w+(?:\s*;\s*(?:var\s+)?\w+(?:\s*,\s*\w+)*\s*:\s*\w+)*))?\s*\))?(?:\s*:\s*(?<ret>\w+))?;";
		private const string TOKEN_BASE = @"(?<tbase>\w+|[{};])|(?<tbase>//)(?:.*$)|(?<tbase>')(?:.*)";
		private const string NORMALIZE = @"(?:{[^}]*})|(?://.*$)";
		private Regex REGEX_TOKENS = new Regex(TOKENS, RegexOptions.IgnoreCase | RegexOptions.Multiline);
		private Regex REGEX_DECLARATION = new Regex(DECLARATION, RegexOptions.IgnoreCase | RegexOptions.Multiline);
		private delegate Statement TokenFoundDelegate(PreparedToken foundToken, int foundAt);
		private string source;
		private IEnumerable<PreparedToken> tokens;
		private int tokensCount = -1;
		private int lastIndex = 0;
		private char[] unuseful = new char[] { '\'', '/', '{', '}' };

		public IEnumerable<Statement> Parse(string source) {
			this.source = source;
			this.tokens = PrepareTokens(source);
			var blocks = ParseRecursive(0, false);

			return blocks;
		}
		private string GetTokenBase(string token) {
			return Regex.Match(token, TOKEN_BASE, RegexOptions.IgnoreCase).Groups["tbase"].Value.ToLower();
		}
		/// <summary>
		/// Get text between two tokens depends on <typeparamref name="RangeSpecifier"/>
		/// </summary>
		/// <param name="from">Starting token</param>
		/// <param name="to">Ending token</param>
		/// <param name="range">Specifies how to crop the string between tokens</param>
		/// <returns></returns>
		private string GetBetween(PreparedToken from, PreparedToken to, RangeSpecifier range = RangeSpecifier.Inner) {
			var startIndex = from == null ? 0 : -1;
			if (startIndex != 0) {
				if (range == RangeSpecifier.Inner)
					startIndex = from.Index + from.Token.Length;
				else if (range == RangeSpecifier.InnerLeft)
					startIndex = from.Index;
			}
			var length = (to == null ? source.Length : to.Index) - startIndex;
			return GetSignificant(source.Substring(startIndex, length));
		}
		/// <summary>
		/// Get text between one token and another neighbor token after of before specified index
		/// </summary>
		/// <param name="index">Base token index</param>
		/// <param name="next">Specifies where to search for another token - after or before</param>
		/// <param name="range">Specifies how to crop the string between tokens</param>
		/// <param name="ignoreUnuseful">Specifies to ignore unuseful tokens like strings and comments</param>
		/// <returns></returns>
		private string GetBetween(int index, bool next, RangeSpecifier range = RangeSpecifier.Inner, bool ignoreUnuseful = true) {
			// FIX: doesn't work properly with inComment when ignoreUnuseful is false
			PreparedToken cToken = tokens.ElementAtOrDefault(index);
			PreparedToken fToken = null;
			bool inComment = false;
			do {
				index = index + (next ? 1 : -1);
				fToken = tokens.ElementAtOrDefault(index);
				if (fToken.Token == "{")
					inComment = next; // depends on way
				else if (fToken.Token == "}")
					inComment = !next;
				if (fToken != null && !inComment && !(ignoreUnuseful && unuseful.Contains(fToken.Token[0])))
					break;
			}
			while (index >= 0 && index < tokensCount);
			//throw new NotImplementedException("check for token inside comment");
			return GetBetween(next ? cToken : fToken, next ? fToken : cToken, range);
		}
		private int GetIndex(int index, bool next, bool ignoreUnuseful = true) {
			// FIX: doesn't work properly with inComment when ignoreUnuseful is false
			PreparedToken token = null;
			bool inComment = false;
			do {
				index = index + (next ? 1 : -1);
				token = tokens.ElementAtOrDefault(index);
				if (token == null)
					continue;
				if (token.Token == "{")
					inComment = next; // depends on way
				else if (token.Token == "}")
					inComment = !next;
				if (!inComment && !(ignoreUnuseful && unuseful.Contains(token.Token[0])))
					break;
			}
			while (index >= 0 && index < tokensCount);
			return index < 0 && index >= tokensCount ? Math.Abs(index) - 1 : index;
		}
		/// <summary>
		/// Removes comments from passed string, normalize and trim it
		/// </summary>
		/// <remarks>
		/// Normalizing meaning formatting string into handy readable form without extra spaces and parentheses
		/// </remarks>
		/// <param name="src">String to be processed</param>
		/// <returns></returns>
		private string GetSignificant(string src) {
			return Regex.Replace(src, NORMALIZE, String.Empty).Trim();
		}
		private Statement ParseUntil(int index, string token, TokenFoundDelegate cb) {
			bool inComment = false;
			PreparedToken tk = null;
			while (index < tokensCount) {
				tk = tokens.ElementAt(index);
				if (tk.Token == token && !inComment)
					return cb(tk, index);
				else if (tk.Token == "{")
					inComment = true;
				else if (tk.Token == "}")
					inComment = false;
				index++;
			}
			throw new Exception(string.Format("Can't find '{0}' statement at {1}",
				token, (tk ?? tokens.ElementAt(tokensCount - 1)).Index));
		}
		private IEnumerable<Statement> ParseRecursive(int index, bool insideScope = true, bool insideBlock = false) {
			if (tokensCount == -1)
				tokensCount = tokens.Count();
			if (index >= tokensCount)	// ? 
				return null;
			var blocks = new List<Statement>();
			Statement block = null;
			var blockCount = 0;
			var insideComment = false;
			do {
				var token = tokens.ElementAt(index);
				// locals
				Statement bl = null;
				bool ignoreBlock = false;
				PreparedToken n_token = null;

				switch (GetTokenBase(token.Token)) {
					case "//":
					case "'":
						continue;
					case "{":
						continue;
					case "function":
					case "procedure":
						#region
						block = new Function();
						bl = ParseUntil(++index, "begin", (t, i) => {
							var decl = ParseDeclaration(GetBetween(token, t, RangeSpecifier.InnerLeft));
							if (decl.Length < 2)
								throw new Exception(string.Format("Can't parse function declaration at {0}", token.Index));
							index = i;
							return new Function() {
								Name = decl[0],
								Text = decl[1]
							};
						});
						(block as Function).Name = (bl as Function).Name;
						block.Text = bl.Text;
						block.Type = "Function";
						(block as Block).Items = new List<Statement>(ParseRecursive(index));
						index = lastIndex;
						break;
						#endregion
					case "begin":

						insideBlock = true;
						break;
					case "end":
						#region
						insideBlock = false;
						lastIndex = GetIndex(index, true);// index + 1;
						if (block != null)
							blocks.Add(block);
						return blocks;
						#endregion
					case ";":
						#region
						if (!insideScope)
							continue;
						block = new Statement();
						block.Text = GetBetween(index, false);
						break;
						#endregion
					case "while":
						#region
						block = new Loop();
						bl = ParseUntil(++index, "do", (t, i) => {
							index = i;
							return new Statement() {
								Text = GetBetween(token, t)
							};
						});
						if (bl == null)
							throw new Exception(string.Format("Can't find 'do' statement at {0}", n_token.Index));
						block.Type = "loop";
						block.Text = bl.Text;
						(block as Block).Items = new List<Statement>(ParseRecursive(index + 1));
						index = lastIndex;
						break;
						#endregion
					case "repeat":
						#region
						block = new Loop(true);
						block.Type = "loop";
						(block as Block).Items = new List<Statement>(ParseRecursive(++index, insideBlock: true));
						bl = ParseUntil(++index, "until", (t, i) => new Statement() {
							Text = GetBetween(t, tokens.ElementAtOrDefault(i + 1)) // replace with GetBetween(i, true)
						});
						block.Text = bl.Text;
						index = GetIndex(lastIndex, true);// lastIndex + 1;
						break;
						#endregion
					case "until":
						#region
						insideBlock = false;
						lastIndex = index;
						if (block != null)
							blocks.Add(block);
						return blocks;
						#endregion
					case "if":
						#region
						block = new Condition();
						bl = ParseUntil(++index, "then", (t, i) => {
							index = i;
							return new Statement() {
								Text = GetBetween(token, t)
							};
						});
						block.Type = "condition";
						block.Text = bl.Text;
						(block as Block).Items = new List<Statement>(ParseRecursive(++index));
						n_token = tokens.ElementAt(lastIndex);
						if (n_token.Token == "else") {
							(block as Condition).ElseItems = new List<Statement>(ParseRecursive(lastIndex + 1));
							/* check if parsed only one empty item
							 * and completely remove 'ElseItems' from result by assignment null
							 * that can happened when using construction like
							 *		if ... then
							 *			if ... then
							 *				foo;
							 *			else
							 *		else
							 *			foo;
							 * PS: why? - maybe casual way of obfuscation
							*/
							if ((block as Condition).ElseItems.Count == 1 && String.IsNullOrEmpty((block as Condition).ElseItems[0].Text))
								(block as Condition).ElseItems = null;
						}
						index = lastIndex;
						break;
						#endregion
					case "else":
						#region
						block = new Statement();
						block.Text = GetBetween(index, false);//tokens.ElementAtOrDefault(index - 1), token);
						blocks.Add(block);
						lastIndex = index;
						return blocks;
						#endregion
					case "for":
						#region
						block = new Loop();
						bl = ParseUntil(++index, "do", (t, i) => {
							index = i;
							return new Statement() {
								Text = GetBetween(token, t)
							};
						});
						block.Text = bl.Text;
						(block as Loop).Items = new List<Statement>(ParseRecursive(index + 1));
						index = lastIndex;
						break;
						#endregion
					case "with":
						#region
						// do no create instance

						//block = new Block();
						// parse just for positioning token, no text needed
						bl = ParseUntil(++index, "do", (t, i) => {
							index = i;
							return new Statement() {
								//Text = GetBetween(token, t)
							};
						});
						//block.Text = bl.Text;

						// insert parsed items into current 'blocks'
						// descr: ignore 'with' statement but processing it's entry
						blocks.AddRange(ParseRecursive(index + 1));
						index = lastIndex;
						break;
						#endregion
					default:
						throw new Exception(string.Format("Unexpected token: '{0}' at {1}", token.Token, token.Index));
				}

				if (block != null) {
					blocks.Add(block);
					block = null;
				}
				if (insideScope && !insideBlock && !ignoreBlock && ++blockCount > 0) {
					lastIndex = index;
					return blocks;
				}

			} while (++index < tokensCount);



			if (block != null)
				blocks.Add(block);
			return blocks;
		}
		private string[] ParseDeclaration(string text) {
			var match = REGEX_DECLARATION.Match(text);
			if (match.Groups["name"] != null && match.Groups["params"] != null)
				return new string[] {
					match.Groups["name"].Value,
					match.Groups["params"].Value
				};
			else
				return new string[] { };
		}
		private IEnumerable<PreparedToken> PrepareTokens(string source) {
			var matches = REGEX_TOKENS.Matches(source);
			return matches.Cast<Match>().Select(m => new PreparedToken() {
				Token = m.Groups["token"].Value,
				Index = m.Groups["token"].Index
			});
		}
	}
}