﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeParser {
	class PreparedToken {
		public string Token { get; set; }
		public int Index { get; set; }
	}
}
