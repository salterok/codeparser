﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeParser {
	class Block : Statement {
		public List<Statement> Items { get; set; }

		public Block() { }
		public Block(string type)
			: base(type) {

		}
	}
}
