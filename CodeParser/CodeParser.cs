﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace CodeParser {
	public class Parser {
		private static readonly Dictionary<string, Type> parsers = null;
		static Parser() {
			parsers = new Dictionary<string, Type>();
			parsers.Add("pas", typeof(PascalParser));
		}
		private IParser GetParser(string lang) {
			Type type = null;
			if (!parsers.TryGetValue(lang, out type))
				throw new Exception(string.Format("Unsupported language: '{0}'", lang));
			return Activator.CreateInstance(type) as IParser;
		}
		public async Task<object> GetSupportedLanguages() {
			throw new NotImplementedException();
			// TODO: convert to json
			return parsers.Keys;
		}
		public async Task<object> Parse(Dictionary<string, object> obj) {
			object source = null;
			object lang = null;
			if (!obj.TryGetValue("source", out source) || !obj.TryGetValue("lang", out lang)) {
				throw new Exception(string.Format("Argument expected: '{0}'", source == null ? "source" : "lang"));
			}
			var parser = GetParser(lang as string);
			var result = parser.Parse(source as string);
			return ConvertToJson(result);
		}

		private object ConvertToJson(IEnumerable<Statement> result) {
			return JsonConvert.SerializeObject(result, Formatting.Indented);
		}
	}
}
